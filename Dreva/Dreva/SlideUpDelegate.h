//
//  SlideUpDelegate.h
//  Dreva
//
//  Created by Eduardo Oliveros Acosta on 14/06/16.
//  Copyright © 2016 Eduardo Oliveros. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SlideUpDelegate <NSObject>

- (void)showSlideUp;
- (void)hideSlideUp;
- (void)moveSlideUp:(UIPanGestureRecognizer *)panGesture;

@end
