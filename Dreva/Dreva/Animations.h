//
//  Animations.h
//  Dreva
//
//  Created by Eduardo Oliveros Acosta on 14/06/16.
//  Copyright © 2016 Eduardo Oliveros. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Animations : NSObject

+(void)modifiqueView:(UIView *)viewToAnimate;

@end
