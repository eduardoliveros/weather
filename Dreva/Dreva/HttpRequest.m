//
//  HttpRequestDelegate.m
//  Dreva
//
//  Created by Eduardo Oliveros Acosta on 14/06/16.
//  Copyright © 2016 Eduardo Oliveros. All rights reserved.
//

#import "HttpRequest.h"

@implementation HttpRequest

#pragma mark init
- (id)initWithURL:(NSString *)newURL {
    self = [super init];
    if(self){
        url = [NSURL URLWithString:newURL];
        self.request = [NSMutableURLRequest requestWithURL:url];
        //[self.request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-Type"];
        // [self.request setValue:@"gzip" forHTTPHeaderField:@"Accept-Encoding"];
    }
    return self;
}

#pragma mark consumeHttpRequest
// action method to consume the http request
-(void)resume{
    [NSURLConnection sendAsynchronousRequest:self.request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               if(connectionError == nil){
                                   [_delegate didCompleteRequest:self.operation receiveData:data];
                               }else{
                                   [_delegate didCompleteRequest:self.operation withError:connectionError];
                               }
                           }];
}
- (void)didCompleteRequest:(int)request withError:(NSError *)error{

}
- (void)didCompleteRequest:(int)request receiveData:(NSData *)data{

}

@end