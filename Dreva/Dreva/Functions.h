//
//  Functions.h
//  Dreva
//
//  Created by Eduardo Oliveros Acosta on 14/06/16.
//  Copyright © 2016 Eduardo Oliveros. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Functions : NSObject


-(void)showError:(NSString *)title subtitle:(NSString *)subtitle;
-(NSString *)returnTemperature:(NSString *)temperature value:(int)value;
-(NSString *)switchTemperature:(NSString *)temperature value:(int)value;
-(NSString *)setupText:(NSString *)city country:(NSString *)country;
@end
