//
//  Functions.m
//  Dreva
//
//  Created by Eduardo Oliveros Acosta on 14/06/16.
//  Copyright © 2016 Eduardo Oliveros. All rights reserved.
//

#import "Functions.h"

@implementation Functions

//show error, change to IOS 9

-(void)showError:(NSString *)title subtitle:(NSString *)subtitle{
    
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:title message:subtitle delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
    
}
//return the temperature in (ºC,ºF)
-(NSString *)returnTemperature:(NSString *)temperature value:(int)value{
    
    int temp = 0;
    switch (value) {
        case 1:
            temp = (temperature.floatValue - 32.0)*(5.0/9.0);
            return [NSString stringWithFormat:@"%d%@",temp,@CELSIUS];
            break;
            
        default:
            temp = temperature.floatValue;
            return [NSString stringWithFormat:@"%d%@",temp,@FAHRENHEIT];
            break;
    }
}
//switch the temperature in (ºC,ºF)
-(NSString *)switchTemperature:(NSString *)temperature value:(int)value{
    
    int temp = 0;
    switch (value) {
        case 1:
            temp = (temperature.floatValue - 32.0)*(5.0/9.0);
            return [NSString stringWithFormat:@"%d%@",temp,@CELSIUS];
            break;
            
        default:
            temp = (temperature.floatValue /(5.0/9.0)) + 32;
            return [NSString stringWithFormat:@"%d%@",temp,@FAHRENHEIT];
            break;
    }
}
//setup the location text, its necesary because in some place the location return null
-(NSString *)setupText:(NSString *)city country:(NSString *)country{
    
    if (city == nil) {
        if (country == nil) {
            return  @"Lugar no establecido";
        }else{
            return country;
        }
    }else{
        if (country == nil) {
            return  city;
        }else{
            return [NSString stringWithFormat:@"%@,%@",city,country];
        }
    }
    
}


@end
