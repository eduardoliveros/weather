//
//  HttpRequestDelegate.h
//  Dreva
//
//  Created by Eduardo Oliveros Acosta on 14/06/16.
//  Copyright © 2016 Eduardo Oliveros. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HttpRequestDelegate <NSObject>

@required

- (void)didCompleteRequest:(int)request withError:(NSError *)error;
- (void)didCompleteRequest:(int)request receiveData:(NSDictionary *)data;

@end
@interface HttpRequest : NSObject <NSURLSessionDelegate, NSURLSessionDataDelegate, NSURLSessionTaskDelegate, NSURLSessionDownloadDelegate> {
    NSURL *url;
    NSMutableData *postBody;
    NSURLSession *session;
}
@property (nonatomic, assign) id <HttpRequestDelegate> delegate;
@property (nonatomic, strong) NSMutableURLRequest *request;
@property (nonatomic) int operation;
- (id)initWithURL:(NSString *)newURL;
-(void)resume;

@end