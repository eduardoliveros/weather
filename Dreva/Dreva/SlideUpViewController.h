//
//  SlideUpViewController.h
//  Dreva
//
//  Created by Eduardo Oliveros Acosta on 14/06/16.
//  Copyright © 2016 Eduardo Oliveros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideUpDelegate.h"
#import "TapViewDelegate.h"

@interface SlideUpViewController : UIViewController <TapViewDelegate>

@property (strong, nonatomic) id <SlideUpDelegate> delegate;
@property (nonatomic) BOOL showingWholeCar;
@property (nonatomic) int unit;
@property (strong, nonatomic) IBOutlet UIView *slideView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@end