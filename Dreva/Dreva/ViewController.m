//
//  ViewController.m
//  Dreva
//
//  Created by Eduardo Oliveros Acosta on 13/06/16.
//  Copyright © 2016 Eduardo Oliveros. All rights reserved.
//

#import "ViewController.h"

//heights of betterPlaces TableView

#define TAP_VIEW_HEIGHT 300.0
#define SLIDE_VIEW_HEIGHT 110.0
#define SLIDE_VIEW_DISTANCE_TO_BUTTON 65.0


@interface ViewController () <UIDynamicAnimatorDelegate>{

}
@property (strong, nonatomic) UIDynamicAnimator *animator;
@property (strong, nonatomic) UISnapBehavior *cashRegisterSnapBehavior;
@property (strong, nonatomic) UIAttachmentBehavior *attachment;
@property (strong, nonatomic) SlideUpViewController *slideUp;
@end

@implementation ViewController {
    
    IBOutlet GMSMapView *mapView_;
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    // flag to allow add more points
    BOOL addPoint;
    // flag to know if the initial location marker is set
    BOOL initialLoc;
    // Variable to know what is the unit set (ºF, ºC)
    int unit;
}

#pragma mark initialMethods

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self initialConfiguration];
    
}

- (void)viewDidAppear:(BOOL)animated{
    // set initial location
    [locationManager startUpdatingLocation];
    UIDynamicItemBehavior * dynamicItem = [[UIDynamicItemBehavior alloc] initWithItems:@[self.containerView]];
    dynamicItem.allowsRotation = NO;
    dynamicItem.elasticity = 0.75;
    [self.animator addBehavior:dynamicItem];
    [self hideSlideUp];
    [self setupDesing];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark setInitialParameters

-(void)initialConfiguration{
    //allow to add point
    addPoint = TRUE;
    //the initial location is not set yet
    initialLoc = FALSE;
    
    // set default unit //fahrenheit (0)
    unit = 0;
    // allow to get ubication user
    
    locationManager = [[CLLocationManager alloc] init];
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    geocoder = [[CLGeocoder alloc] init];
    
    //setup mapview
    mapView_.delegate = self;
    
    
}
#pragma mark -buttonDesing
-(void)setupDesing{
    
    //border and corner
    [Appareance setBorderButton:self.buttonSelect];
    [Appareance setBorderButton:self.buttonLocation];
    [Appareance setBorderButton:self.buttonCeSe];
    [Appareance setBorderButton:self.buttonFaSe];
    
    //animation to explain to the user, that he can use it
    
    [Animations modifiqueView:self.buttonSelect];
    [Animations modifiqueView:self.buttonLocation];
    
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    if (addPoint) {
        // add first dreamer Vacation place
        [self getCity:[[CLLocation alloc]initWithLatitude:coordinate.latitude longitude:coordinate.longitude] type:DREAM_STATUS addMarker:TRUE];
        // Dont allow anymore to add more points
        addPoint = FALSE;
        
    }else{
        
        [[Functions alloc] showError:@"No puedes agregar mas direcciones" subtitle:@"arrastra el marcador actual a la zona que quieras consultar"];
    }
}


-(void)addMarker:(CLLocation *)location title:(NSString *)title type:(int)type{
    
    //setup camera map
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude
                                                            longitude:location.coordinate.longitude
                                                                 zoom:6];
    mapView_.camera = camera;
    mapView_.myLocationEnabled = YES;
    
    // Creates a marker in the center of the map.
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = location.coordinate;
    marker.title = title;
    
    //set color depends of the marker type
    switch (type) {
        case INITIAL_STATUS:
            marker.icon = [GMSMarker markerImageWithColor:[UIColor blackColor]];
            break;
        case DREAM_STATUS:
            marker.icon = [GMSMarker markerImageWithColor:[UIColor orangeColor]];
            break;
        default:
            break;
    }
    //set user data, allow to the user to edit the marker in the future
    
    marker.userData = @{@"id":[NSString stringWithFormat:@"%d",type]};
    marker.map = mapView_;
    marker.draggable = true;
}
#pragma mark markersAction

-(void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker{
   
   [self getCity:[[CLLocation alloc]initWithLatitude:marker.position.latitude longitude:marker.position.longitude] type:[marker.userData[@"id"] intValue] addMarker:FALSE];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    [[Functions alloc] showError:@"Lo Sentimos" subtitle:@"No fue Posible obtener tu ubicación"];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        NSLog(@"latitude %@",[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude]);
        NSLog(@"longitude %@",[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude]);
    }
    [locationManager stopUpdatingLocation];
    [self getCity:currentLocation type:INITIAL_STATUS addMarker:TRUE];
    initialLoc = TRUE;
}

#pragma mark geocoder
//get the place text with geocoder

-(void)getCity:(CLLocation*)currentLocation type:(int)type addMarker:(BOOL)addmarker{
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        if (error == nil && [placemarks count] > 0) {
            placemark = [placemarks lastObject];
            NSString *place = [[Functions alloc]setupText:placemark.locality country:placemark.country];
            switch (type) {
                case INITIAL_STATUS:
                    self.labelCity2.hidden = FALSE;
                    self.labelCity2.text = place;
                    break;
                case DREAM_STATUS:
                    self.labelCity1.hidden = FALSE;
                    self.labelCity1.text = place;
                    break;
                default:
                    break;
            }
            if(addmarker){
                [self addMarker:currentLocation title:placemark.locality type:type];
            }
            [self getWheather:currentLocation type:type];
            
        } else {
            NSLog(@"%@", error.debugDescription);
        }
    } ];
}
#pragma mark wheather

-(void)getWheather:(CLLocation*)location type:(int)type{
    HttpRequest *request = [[HttpRequest alloc]initWithURL:[NSString stringWithFormat:@"https://api.forecast.io/forecast/%s/%f,%f",API_WEATHER,location.coordinate.latitude,location.coordinate.longitude]];
    request.operation = type;
    request.delegate = self;
    [request resume];
}

-(void)defineTheBetterOption{
    
    if (self.labelTemperature1.text.integerValue > self.labelTemperature2.text.integerValue) {
        self.labelTemperature1.textColor = [UIColor greenColor];
        self.labelTemperature2.textColor = [UIColor redColor];
        [[Functions alloc]showError:[NSString stringWithFormat:@"Viaja a %@",self.labelCity1.text] subtitle:@"El clima es mas alto!"];
    }else{
        self.labelTemperature2.textColor = [UIColor greenColor];
        self.labelTemperature1.textColor = [UIColor redColor];
        [[Functions alloc]showError:[NSString stringWithFormat:@"Viaja a %@",self.labelCity2.text] subtitle:@"El clima es mas alto!"];
    }
}
#pragma mark HttpRequestDelegate

- (void)didCompleteRequest:(int)request withError:(NSError *)error{
    [[Functions alloc] showError:@"Lo Sentimos" subtitle:@"No fue calcular la temperatura, vuelve a intentar"];
}
- (void)didCompleteRequest:(int)request receiveData:(NSData *)data{
    __block NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                           options:0
                                             error:nil];
    //get currently data
    NSData *currentData = [json valueForKey:@"currently"];
    //get temperature, the api return the temperature in fahrenheigth units
    NSString *temperature = [NSString stringWithFormat:@"%@",[[Functions alloc]returnTemperature:[currentData valueForKey:@"temperature"] value:unit]];
    
    switch (request) {
        case INITIAL_STATUS:
            self.labelTemperature2.hidden = FALSE;
            self.labelTemperature2.text = temperature;
            break;
        case DREAM_STATUS:
            self.labelTemperature1.hidden = FALSE;
            self.labelTemperature1.text =  temperature;
            break;
        default:
            break;
    }
    // Set the better option to travel if both places was defined
    if(initialLoc && !addPoint){
        [self defineTheBetterOption];
    }
}
#pragma mark buttonActions
- (IBAction)getCurrentLocation:(id)sender {
    // set initial location
    [locationManager startUpdatingLocation];
}
- (IBAction)changeUnit:(id)sender {
    [self hideButtons];
}
- (IBAction)changeToFa:(id)sender {
    unit = 0;
    [self.buttonSelect setTitle:@FAHRENHEIT forState:UIControlStateNormal];
    [self hideButtons];
    [self changeUnit];
    
}
- (IBAction)changeToCe:(id)sender {
        [self.buttonSelect setTitle:@CELSIUS forState:UIControlStateNormal];
    unit = 1;
    [self hideButtons];
    [self changeUnit];
}

#pragma mark changeButtonAttributes

-(void)changeUnit{
    //set unit in labels (TºC,TºF)
    
    self.labelTemperature1.text = [NSString stringWithFormat:@"%@",[[Functions alloc]switchTemperature:self.labelTemperature1.text value:unit]];
    self.labelTemperature2.text = [NSString stringWithFormat:@"%@",[[Functions alloc]switchTemperature:self.labelTemperature2.text value:unit]];
    //set unit in Better places
    _slideUp.unit = unit;
    [_slideUp.tableView reloadData];
    
}
-(void)hideButtons{
    self.buttonCeSe.hidden = !self.buttonCeSe.hidden;
    self.buttonFaSe.hidden = !self.buttonFaSe.hidden;
    
}
#pragma mark - Animations

/** Animates up the shopping car so that it covers all the screen */
- (void)animateUpShoppingCar {
    _slideUp.unit = unit;
    [self.animator removeBehavior:self.cashRegisterSnapBehavior];
    
    self.cashRegisterSnapBehavior = [[UISnapBehavior alloc] initWithItem:self.containerView snapToPoint:CGPointMake(self.view.frame.size.width / 2,self.view.frame.size.height - SLIDE_VIEW_DISTANCE_TO_BUTTON)];
    self.cashRegisterSnapBehavior.damping = 0.3;
    [self.animator addBehavior: self.cashRegisterSnapBehavior];
}

/** Animates down the shopping so that only the cash register is showing */
- (void)animateDownShoppingCar {
    [self.animator removeBehavior:self.cashRegisterSnapBehavior];
    self.cashRegisterSnapBehavior = [[UISnapBehavior alloc] initWithItem:self.containerView snapToPoint:CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height + SLIDE_VIEW_HEIGHT)];
    self.cashRegisterSnapBehavior.damping = 0.3;
    [self.animator addBehavior: self.cashRegisterSnapBehavior];
}
#pragma mark SlideUpShoppingCarDelegate Methods
- (void)showSlideUp{
    [self animateUpShoppingCar];
}
- (void)hideSlideUp {
    [self animateDownShoppingCar];
}
- (void)moveSlideUp:(UIPanGestureRecognizer *)panGesture{
    CGPoint gesturePoint = [panGesture locationInView:self.view];
    if (panGesture.state == UIGestureRecognizerStateBegan) {
        self.attachment = [[UIAttachmentBehavior alloc] initWithItem:self.containerView attachedToAnchor:CGPointMake(self.view.frame.size.width / 2, gesturePoint.y)];
        self.attachment.length = self.containerView.frame.size.height / 2 - TAP_VIEW_HEIGHT / 2;
        [self.animator addBehavior:self.attachment];
        [self.animator removeBehavior:self.cashRegisterSnapBehavior];
    } else if (panGesture.state == UIGestureRecognizerStateChanged) {
        self.attachment.anchorPoint = CGPointMake(self.view.frame.size.width / 2, gesturePoint.y);
    } else if (panGesture.state == UIGestureRecognizerStateEnded) {
        [self.animator removeBehavior:self.attachment];
        if (gesturePoint.y <= self.view.frame.size.height / 2) {
            [self showSlideUp];
            self.slideUp.showingWholeCar = YES;
        } else {
            [self hideSlideUp];
            self.slideUp.showingWholeCar = NO;
        }
    }
}
#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Embed SlideUpViewController"]) {
        NSLog(@"Entró al segue");
        //Embed the SlideUpShoppinCarViewController
        SlideUpViewController *dvc = (SlideUpViewController *)segue.destinationViewController;
        dvc.delegate = self;
        //        dvc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        self.slideUp = dvc;
    }
    
}
# pragma mark - Lazy initializations & Setters

- (UIDynamicAnimator *)animator {
    if (!_animator) {
        _animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
        _animator.delegate = self;
    }
    return _animator;
}


@end
