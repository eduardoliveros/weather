//
//  SlideUpViewController.m
//  Dreva
//
//  Created by Eduardo Oliveros Acosta on 14/06/16.
//  Copyright © 2016 Eduardo Oliveros. All rights reserved.
//


#import "SlideUpViewController.h"
#import "TapView.h"
#import "HttpRequest.h"

#define TAP_VIEW_HEIGHT 300.0

@interface SlideUpViewController ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,HttpRequestDelegate>{
    
    UITapGestureRecognizer *tap;
    NSMutableArray *betterPlaces;
    NSArray *cities;
}
@property (strong, nonatomic) TapView *tapView;

@end

@implementation SlideUpViewController


- (void)viewDidLoad {
    [super viewDidLoad];    // Do any additional setup after loading the view.
    betterPlaces = [[NSMutableArray alloc]init];
    self.showingWholeCar = NO;
    //initial cities, it should be come from http Service, it array is provitional
    cities = @[@"Ciudad de Panama",@"Lima"];
    
    
}
- (void)viewDidAppear:(BOOL)animated {
    [self.slideView addSubview:[self addCashRegister]];
    [super viewDidAppear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
    //get the wheather of to points, this points should will change, it should be come from HTTPService, it points are provitional
    [self getWheather:9.084555 longitude:79.408384 index:0];
    [self getWheather:-12.048627 longitude:-77.033685 index:1];
}

#pragma mark wheader
-(void)getWheather:(float)latitude longitude:(float)longitude index:(int)index{
    HttpRequest *request = [[HttpRequest alloc]initWithURL:[NSString stringWithFormat:@"https://api.forecast.io/forecast/%s/%f,%f",API_WEATHER,latitude,longitude]];
    request.operation = index;
    request.delegate = self;
    [request resume];
}



#pragma mark - CashRegisterActions

/** Adds a CashRegisterView to the bottom of the view*/
- (TapView *)addCashRegister {
    TapView *tapView = [[TapView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, TAP_VIEW_HEIGHT)];
    tapView.delegate = self;
    [self.view addSubview:tapView];
    self.tapView = tapView;
    return tapView;
}

#pragma mark - CashRegisterDelegate Methods

-(void)tappedCashRegister {
    
    if (!self.showingWholeCar) {
        [self.delegate showSlideUp];
        self.showingWholeCar = YES;
    } else {
        self.showingWholeCar = NO;
        [self.delegate hideSlideUp];
    }
}

-(void)pannedCashRegister:(UIPanGestureRecognizer *)pangesture {
    [self.delegate moveSlideUp:pangesture];
}

#pragma mark tableviewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return betterPlaces.count;
    
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    
    return 1;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    NSDictionary *dict = [betterPlaces objectAtIndex:indexPath.row];
    cell.textLabel.text = dict[@"temperature"];
    cell.detailTextLabel.text = dict[@"place"];
    return cell;
}
#pragma mark HttpRequestDelegate

- (void)didCompleteRequest:(int)request withError:(NSError *)error{
    [[Functions alloc] showError:@"Lo Sentimos" subtitle:@"No fue calcular la temperatura, vuelve a intentar"];
}
- (void)didCompleteRequest:(int)request receiveData:(NSData *)data{
    __block NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:0
                                                                   error:nil];
    //get currently data
    NSData *currentData = [json valueForKey:@"currently"];
    //get temperature, the api return the temperature in fahrenheigth units
    NSString *temperature = [NSString stringWithFormat:@"%@",[[Functions alloc]returnTemperature:[currentData valueForKey:@"temperature"] value:_unit]];
    NSDictionary *dict = @{@"temperature":temperature,@"place":cities[request]};
    [betterPlaces addObject:dict];
    [self.tableView reloadData];

    
}

@end
