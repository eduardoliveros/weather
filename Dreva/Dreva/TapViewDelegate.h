//
//  CashRegisterDelegate.h
//  Dreva
//
//  Created by Eduardo Oliveros Acosta on 14/06/16.
//  Copyright © 2016 Eduardo Oliveros. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TapViewDelegate <NSObject>
/** Tells the delegate the user tapped the cash register */
- (void)tappedCashRegister;
- (void)pannedCashRegister:(UIPanGestureRecognizer *)panGesture;
@end
