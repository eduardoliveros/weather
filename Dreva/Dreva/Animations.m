//
//  Animations.m
//  Dreva
//
//  Created by Eduardo Oliveros Acosta on 14/06/16.
//  Copyright © 2016 Eduardo Oliveros. All rights reserved.
//

#import "Animations.h"

@implementation Animations

// animation to put bigger and return to the initial size en 0.5

+(void)modifiqueView:(UIButton *)viewToAnimate{
    
    [UIView animateWithDuration:0.5 animations:^{
        [viewToAnimate setTransform:CGAffineTransformMakeScale(1.5, 1.5)];
    }completion:^(BOOL finished) {
        [viewToAnimate setTransform:CGAffineTransformMakeScale(1.0, 1.0)];
    }];
}

@end
