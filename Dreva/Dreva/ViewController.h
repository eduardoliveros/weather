//
//  ViewController.h
//  Dreva
//
//  Created by Eduardo Oliveros Acosta on 13/06/16.
//  Copyright © 2016 Eduardo Oliveros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>
#import "HttpRequest.h"
#import "SlideUpViewController.h"

@interface ViewController : UIViewController<CLLocationManagerDelegate, GMSMapViewDelegate, HttpRequestDelegate,SlideUpDelegate>

@property (strong, nonatomic) IBOutlet UIButton *buttonLocation;
@property (strong, nonatomic) IBOutlet UILabel *labelTemperature1;
@property (strong, nonatomic) IBOutlet UILabel *labelCity1;
@property (strong, nonatomic) IBOutlet UILabel *labelTemperature2;
@property (strong, nonatomic) IBOutlet UILabel *labelCity2;
@property (strong, nonatomic) IBOutlet UIButton *buttonFaSe;
@property (strong, nonatomic) IBOutlet UIButton *buttonCeSe;
@property (strong, nonatomic) IBOutlet UIButton *buttonSelect;
@property (strong, nonatomic) IBOutlet UIView *containerView;


@end

