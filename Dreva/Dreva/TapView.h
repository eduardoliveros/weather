//
//  CashRegister.h
//  Dreva
//
//  Created by Eduardo Oliveros Acosta on 14/06/16.
//  Copyright © 2016 Eduardo Oliveros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TapViewDelegate.h"

@interface TapView : UIView
@property (strong, nonatomic) NSString *cashLabelText;
@property (strong, nonatomic) id<TapViewDelegate> delegate;

@end