//
//  Appareance.m
//  Dreva
//
//  Created by Eduardo Oliveros Acosta on 14/06/16.
//  Copyright © 2016 Eduardo Oliveros. All rights reserved.
//

#import "Appareance.h"

@implementation Appareance

//method to set circular desing with border to buttons

+(BOOL)setBorderButton:(UIButton *)button{
    button.layer.cornerRadius = 5;
    button.clipsToBounds = YES;
    [button.layer setBorderColor: [[UIColor colorWithRed:225/255.0 green:9/255.0 blue:57/255.0 alpha:1.0] CGColor]];
    [button.layer setBorderWidth: 1.0];
    [button.layer setCornerRadius:button.frame.size.height / 2];
    [button.layer setMasksToBounds:YES];
    
    return YES;
}

@end
