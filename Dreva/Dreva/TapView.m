//
//  CashRegister.m
//  Dreva
//
//  Created by Eduardo Oliveros Acosta on 14/06/16.
//  Copyright © 2016 Eduardo Oliveros. All rights reserved.
//

#import "TapView.h"

@interface TapView()
@property (strong, nonatomic) UILabel *priceLabel;
@end

@implementation TapView

# pragma mark - Initializations

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

# pragma mark - Setup
/** Sets up the CashRegisterView */
- (void) setup {

    [self addGestures];
}

- (void)addGestures{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tellDelegateCashRegisterWasTouched:)];
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(tellDelegateCashRegisterWasPanned:)];
    
    [self addGestureRecognizer:tapGesture];
    [self addGestureRecognizer:panGesture];
}


# pragma mark - Gestures

/** Tells the delegate that the Cash Register was touched */
- (void)tellDelegateCashRegisterWasTouched:(UITapGestureRecognizer *)gesture {
    [self.delegate tappedCashRegister];
}

/** Tells the delegate that the Cash Register was swiped */
- (void)tellDelegateCashRegisterWasPanned:(UIPanGestureRecognizer *)panGesture {
    [self.delegate pannedCashRegister:panGesture];
}
@end

